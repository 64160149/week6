package com.saharat.week6;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;

public class RobotTest {
    @Test
    public void shouldCreateRobotSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY());
    }

    @Test
    public void shouldRobotUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldRobotUpFail() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN);
        boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(Robot.Y_MIN, robot.getY());
    }


    @Test
    public void shouldRobotDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldRobotDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(16, robot.getY());
    }

    @Test
    public void shouldRobotDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(8);
        assertEquals(true, result);
        assertEquals(19, robot.getY());
    }

    @Test
    public void shouldRobotDownNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.down(9);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }


    @Test
    public void shouldRobotDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX-1);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }

    @Test
    public void shouldRobotDownAtMax() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY());
    }


    @Test
    public void shouldRobotLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 9, 10);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(8, robot.getX());
    }

    @Test
    public void shouldRobotLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 9, 10);
        boolean result = robot.left(5);
        assertEquals(true, result);
        assertEquals(4, robot.getX());
    }

    @Test
    public void shouldRobotLeftNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 9, 10);
        boolean result = robot.left(9);
        assertEquals(true, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRobotLeftNFail1() {
        Robot robot = new Robot("Robot", 'R', 9, 10);
        boolean result = robot.left(12);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    @Test
    public void shouldRobotRightSuccess() {
        Robot robot = new Robot("Robot", 'R', 11, 10);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(12, robot.getX());
    }
    
    @Test
    public void shouldRobotRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 11, 10);
        boolean result = robot.right(5);
        assertEquals(true, result);
        assertEquals(16, robot.getX());
    }

    @Test
    public void shouldRobotRightNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 11, 10);
        boolean result = robot.right(8);
        assertEquals(true, result);
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldRobotRightNFail1() {
        Robot robot = new Robot("Robot", 'R', 11, 10);
        boolean result = robot.right(9);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldRobotLeftFail() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MIN, 10);
        boolean result = robot.left();
        assertEquals(false, result);
        assertEquals(Robot.X_MIN, robot.getX());
    }

    
    @Test
    public void shouldRobotRightFail() {
        Robot robot = new Robot("Robot", 'R', Robot.X_MAX, 10);
        boolean result = robot.right();
        assertEquals(false, result);
        assertEquals(Robot.X_MAX, robot.getX());
    }









}
