package com.saharat.week6;

import static org.junit.Assert.assertEquals;

import java.lang.annotation.Target;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldDepositSuccess() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100.0, book.getBalance(), 0.00001);

    }

    @Test
    public void shouldDepositNegative() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(-100);
        assertEquals(false, result);
        assertEquals(0, book.getBalance(), 0.00001);
        
    }

    @Test
    public void shouldWitDrawSuccess() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.getBalance(), 0.00001);
        
    }

    @Test
    public void shouldDepositNegative2() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.deposit(-50);
        assertEquals(false, result);
        assertEquals(100, book.getBalance(), 0.00001);
        
    }

    @Test
    public void shouldWitDrawOverBalnce() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWitDrawSuccess100() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(0, book.getBalance(), 0.00001);
    }
}

