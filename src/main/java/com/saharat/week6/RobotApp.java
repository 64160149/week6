package com.saharat.week6;

public class RobotApp {
    public static void main(String[] args) {
        Robot body = new Robot("Body" , 'B', 1, 0);
        Robot Peter = new Robot("Peter", 'P', 10, 10);
        body.print();
        body.right();
        body.left();
        body.down();
        body.down();
        body.right();
        body.right();
        body.down();
        body.left();
        body.print();

        Peter.print();
        Peter.right();
        Peter.down();
        Peter.down();
        Peter.right();
        Peter.up();
        Peter.left();
        Peter.print();
        
        for (int y = Robot.Y_MIN; y < Robot.Y_MAX; y++) {
            for (int x = Robot.X_MIN; x < Robot.X_MAX; x++) {
                if(body.getX() == x && body.getY() == y) {
                    System.out.print(body.getSymbol());
                } else if(Peter.getX() == x && body.getY() == y) {
                    System.out.println(Peter.getSymbol());
                } else {
                    System.out.println("-");
                }
                
            }
            System.out.println();
        }
    }

}
