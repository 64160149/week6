package com.saharat.week6;


public class BookBankApp {
    public static void main(String[] args) {
        BookBank saharat = new BookBank("Phossil", 100.0);
        saharat.print();
        saharat.deposit(50);
        saharat.print();
        saharat.withdraw(50);
        saharat.print();

        BookBank merai = new BookBank("Sickadui", 1);
        merai.deposit(1000000);
        merai.withdraw(10000000);
        merai.print();
        
        BookBank tommai = new BookBank("Reywon", 10);
        tommai.deposit(10000000);
        tommai.withdraw(1000000);
        tommai.print();
    }
}
